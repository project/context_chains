<?php

namespace Drupal\Tests\context_chains\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Defines a class for testing chained context provision.
 *
 * @group context_chains
 */
class ContextChainsProviderTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'context_chains',
    'node',
    'taxonomy',
    'filter',
    'text',
    'options',
    'context_chains_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installEntitySchema('taxonomy');
    $this->installConfig('node');
    $this->installConfig('context_chains_test');
  }

  /**
   * Tests context chaining.
   */
  public function testChaining() {
    $context_repository = $this->container->get('context.repository');
    $contexts = $context_repository->getAvailableContexts();
    $this->assertArrayHasKey('@context_chains.chain:node:page:tags', $contexts);
    $this->assertSame('entity:taxonomy_term', $contexts['@context_chains.chain:node:page:tags']->getContextDefinition()->getDataType());
  }

}
