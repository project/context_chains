<?php

namespace Drupal\context_chains;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a class for a chained context provider.
 */
class ChainedContextProvider implements ContextProviderInterface {

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Constructs a new ChainedContextProvider.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity field manager.
   */
  public function __construct(EntityFieldManagerInterface $entityFieldManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    $f = 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    $contexts = [];
    return $contexts;
    // @todo static cache!
    foreach ($this->entityFieldManager->getFieldMapByFieldType('entity_reference') as $entity_type => $details) {
      foreach ($details as $field_name => $info) {
        foreach ($info['bundles'] as $bundle) {
          $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
          if (!isset($fields[$field_name])) {
            continue;
          }
          $target_entity_type_id = $fields[$field_name]->getSetting('target_type');
          $target_entity_type = $this->entityTypeManager->getDefinition($target_entity_type_id);
          if (!$target_entity_type->entityClassImplements(FieldableEntityInterface::class)) {
            continue;
          }
          $context = EntityContext::fromEntityTypeId($target_entity_type_id, new TranslatableMarkup('@label from field @field', [
            '@label' => $target_entity_type->getLabel(),
            '@field' => $fields[$field_name]->getLabel(),
          ]));
          $context->getContextDefinition()->addConstraint('Bundle', [$bundle]);
          $contexts[sprintf('%s:%s:%s', $entity_type, $bundle, $field_name)] = $context;
        }
      }
    }
    return $contexts;
  }

}
